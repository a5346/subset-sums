from typing import List
from copy import deepcopy


def find_subset_sum(__list: List[int], __sum: int) -> bool:
    if sum(__list) == __sum:
        return True

    for i, number in enumerate(__list):
        subset = deepcopy(__list)
        subset.pop(i)

        if find_subset_sum(subset, __sum):
            return True

    return False


def main() -> None:
    lst = [3, 6, 2, 7, 2]
    assert not find_subset_sum(lst, 25)
    assert find_subset_sum(lst, 15)
    assert find_subset_sum([1, -2, 1, 1, 2], 0)


if __name__ == '__main__':
    main()
